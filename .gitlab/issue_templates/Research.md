# Goals

- [ ] Define research goals as tasks.

# Outcomes

- Explain learnings here.

<!-- Remember to mention any relevant issues! -->

/assign @erran
/confidential
/health_status on_track
/label ~"Incubation Engineering Department" ~"Breach and Attack Simulation"
