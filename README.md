# Meta
This project is used for tracking research, roadmap updates, and weekly updates for the [Breach and Attack Simulation Single-Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/breach-and-attack-simulation/).

1. [Open a new research issue here.](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/new?issuable_template=Research&issue[title]=%5BResearch%5D%20XXX)
1. [Open a new roadmap issue here.](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/new?issuable_template=Roadmap)
1. [Open a new weekly update issue here.](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/new?issuable_template=Weekly%20Update%20YYYY-MM-DD&issue[title]=Weekly%20Update%202023-MM-DD)

## Research

[Research issues](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/?search=%5BResearch%5D) should be marked as confidential so they remain [internal](https://about.gitlab.com/handbook/communication/confidentiality-levels/#internal) but accessible to all team members.

## Roadmap

[Roadmap issues](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues) should be [public](https://about.gitlab.com/handbook/communication/confidentiality-levels/#public-by-default).

## Weekly Updates

[Weekly Updates issues](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/?search=Weekly%20Update) should be [public](https://about.gitlab.com/handbook/communication/confidentiality-levels/#public-by-default).

This [summary issue](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/meta/-/issues/1) should also be updated automatically to include a running list of issues and youtube videos.
